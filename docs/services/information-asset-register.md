# Information Asset Register

This page documents key information about the Information Asset Register service.

Environments and Servers they run on
------------------------------------

- [Production](https://iar.admin.cam.ac.uk/) (and [API](https://iar-backend.gcloud.automation.uis.cam.ac.uk/ui/))
    - [Application: GCP Kubernetes Cluster](https://console.cloud.google.com/kubernetes/list?project=information-asset-register)
    - [Authorisation: GCP Kubernetes Cluster](https://console.cloud.google.com/kubernetes/list?project=uis-automation-infrastructure)
- [Test](https://iar-test.gcloud.automation.uis.cam.ac.uk/) (and [API](https://iar-backend-test.gcloud.automation.uis.cam.ac.uk/ui/))
    - [GCP Kubernetes Cluster (same as production)](https://console.cloud.google.com/kubernetes/list?information-asset-register)

Application repositories
------------------------
- [Backend for IAR web application](https://github.com/uisautomation/iar-backend)
- [Frontend for IAR web application](https://github.com/uisautomation/iar-frontend)
- [Gather statistics for the IAR](https://github.com/uisautomation/iar-gatherstats)
- [Hydra Consent App](https://github.com/uisautomation/hydra-consent-app)
- [Lookup proxy API](https://github.com/uisautomation/lookupproxy)

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | Python 3.6 | Django 2.0 |
| Client | JavaScript | React 16.2 |

Deployment
----------
- [Application: GCP Kubernetes Cluster](https://console.cloud.google.com/kubernetes/list?project=information-asset-register)
- [Authorisation: GCP Kubernetes Cluster](https://console.cloud.google.com/kubernetes/list?project=uis-automation-infrastructure)
- [Lookup proxy API: Internal Docker Swarm Cluster](https://portainer.swarm.usvc.gcloud.automation.uis.cam.ac.uk)

Deployment repository
---------------------
- [Application](https://github.com/uisautomation/iar-deploy)
- [OAuth2 endpoint](https://gitlab.developers.cam.ac.uk/uis/devops/iar/experimental-oauth2-deploy)
- [Lookup proxy API](https://gitlab.developers.cam.ac.uk/uis/devops/usvc-deploy)

Service Owner
-------------
[Vijay Samtani](https://www.lookup.cam.ac.uk/person/crsid/vkhs1)

Service Managers
----------------
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)

Current Status
--------------
Alpha

Documentation
-------------
- [Backend for IAR web application](https://uisautomation.github.io/iar-backend/)
