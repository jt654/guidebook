# GSuite@Cambridge

This page documents key information about the GSuite@Cambridge service. 

Environments and Servers they run on
------------------------------------

- [Production](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/sync-deploy/pipelines) uses G-Suite for Education at the `cam.ac.uk` domain.
- No test sync but the we have a test G-Suite for Education at the `gdev.csi.cam.ac.uk` domain.

Application repositories
------------------------
- [Operational Documentation and Sync tool](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite)

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| [Directory Sync tool](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/synctool) | Python | Scripts using `google-api-python-client` and `google-auth` libraries |

Deployment
----------
The Directory Sync tool is executed periodically by [Gitlab CI](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/sync-deploy/pipelines)

Deployment repository
---------------------
- [Gilab CI Sync job repo](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/sync-deploy)

Service Owner
-------------
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)

Service Managers
----------------
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)

Current Status
--------------
Live

Documentation
-------------
- [Operational Documentation](https://gitlab.developers.cam.ac.uk/uis/devops/gsuite/operational-documentation)
- [Public Documentation](https://help.uis.cam.ac.uk/service/collaboration/g-suite)

- [Operational Documentation (Out of date)](https://wiki.cam.ac.uk/ucs/Google_Apps)
- [Service Desk Documentation (Out of date)](https://wiki.cam.ac.uk/uis/Service_Desk_Knowledgebase:_Google_Apps_@_Cambridge)
