# Research Dashboard

This page documents key information about the Research Dashboard service.

Environments and Servers they run on
------------------------------------
- [Production](https://researchdashboard.admin.cam.ac.uk)
    - [GCP Kubernetes Cluster](https://console.cloud.google.com/kubernetes/list?project=uis-automation-pidash)
- [Test](https://test.researchdashboard.admin.cam.ac.uk)
    - [GCP Kubernetes Cluster (same as production)](https://console.cloud.google.com/kubernetes/list?project=uis-automation-pidash)

Application repositories
------------------------
- [Web Application](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/webapp)
- [CUFS GMS Microservice](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/datamart-api)
- [CHRIS Grant Contracts Microservice](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/chris-salaries-api)
- [CUFS Salary Commitments Calculator](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/salaries-commitments)

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | Python 3.7 | Django 2.1 |
| Client | TypeScript | React 16.4 |

Deployment
----------
[GCP Kubernetes Cluster](https://console.cloud.google.com/kubernetes/list?project=uis-automation-pidash)

Deployment repository
---------------------
[Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/research-dashboard/deployment)

Service Owner
-------------
[Dawn Edwards](https://www.lookup.cam.ac.uk/person/crsid/dgb26)

Service Managers
----------------
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203) / 
[Will Russell](https://www.lookup.cam.ac.uk/person/crsid/weer2)

Current Status
--------------
Beta

Documentation
-------------
- [Main documentation](https://wiki.cam.ac.uk/uis/PI_Dashboard)
