# Self Service Gateway (and Dropbox)

This page documents key information about the Self Service Gateway and Dropbox services.

Environments and Servers they run on
------------------------------------
- [Production](https://selfservice.uis.cam.ac.uk/)
    - selfservice-node1.srv.uis.private.cam.ac.uk
    - selfservice-node2.srv.uis.private.cam.ac.uk
    - selfservice-node3.srv.uis.private.cam.ac.uk
    - selfservice-node4.srv.uis.private.cam.ac.uk
    - selfservice-nodeq.srv.uis.private.cam.ac.uk
- [Test](https://test.selfservice.uis.cam.ac.uk/)
    - sstest-node1.srv.uis.private.cam.ac.uk
    - sstest-node2.srv.uis.private.cam.ac.uk
    - sstest-nodeq.srv.uis.private.cam.ac.uk

Application repositories
------------------------
 - [Web Application](https://gitlab.developers.cam.ac.uk/uis/devops/ssgw/webapp)
 - [CUFS API Microservice](https://gitlab.developers.cam.ac.uk/uis/devops/ssgw/cufs-api)
 - [Secure Trading API Microservice](https://gitlab.developers.cam.ac.uk/uis/devops/ssgw/securetrading)

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | Python 3.6 | Django 1.11 <br> Celery 3.1 |
| Client | JavaScript | jQuery 1.11.1 |

Deployment
----------
Docker/systemd cluster distributed over internal VMs.

Deployment repository
---------------------
[Ansible Deployment](https://gitlab.developers.cam.ac.uk/uis/devops/ssgw/ansible)

Service Owner
-------------
[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)

Service Managers
----------------
[Mike Bamford](https://www.lookup.cam.ac.uk/person/crsid/mb2174)

Current Status
--------------
Live

Documentation
-------------
- [Main documentation](https://wiki.cam.ac.uk/uis/Self-service_Gateway)
- [Payment Methods (at the moment)](https://gitlab.developers.cam.ac.uk/uis/devops/ssgw/docs)
