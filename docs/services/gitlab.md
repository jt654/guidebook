# GitLab service for the Developers' Hub

This page documents key information about the GitLab service for the University
Developers' Hub.

!!! note
    The Developers' Hub is a University-wide project to foster a sense of
    community among software developers.

Environments
------------

The production GitLab service is available at
[gitlab.developers.cam.ac.uk](https://gitlab.developers.cam.ac.uk/).

Test environments are spun up on demand to test changes to the deployment. More
information is available in the [deployment project
documentation](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/deploy/blob/master/docs/bootstrap.md)
(DevOps only).

Deployment repositories
-----------------------

- [Deployment repository](https://github.com/uisautomation/gitlab-deploy).
    (DevOps only. Unlike other services, the "canonical" repository for GitLab is hosted
    outside of GitLab since we need to retain access in the event of an outage.)
- [GitLab mirror of deployment
    project](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/deploy).
    (DevOps only)

Deployment
----------

Deployment is via a terraform configuration hosted in the deployment repository.

Service Owner
-------------

[Abraham Martin](https://www.lookup.cam.ac.uk/person/crsid/amc203)

Service Managers
----------------

[Rich Wareham](https://www.lookup.cam.ac.uk/person/crsid/rjw57)

Current Status
--------------

Beta

Documentation
-------------

- [User-facing
    documentation](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs/wikis/FAQs).
- [User support
    project](https://gitlab.developers.cam.ac.uk/uis/devops/devhub/docs/)
