# Jackdaw

This page documents key information about the Jackdaw service.

Environments and Servers they run on
------------------------------------

#### Production Server
- cname jackdaw.cam.ac.uk
- floating ip knot.csi.cam.ac.uk (alias on the network interface on the host currently the master production server)
#### Hosts
  - skua.csi
  - owl.csi
  - ruff.csi

#### URL pattern
- https://jackdaw.cam.ac.uk[:PORT]/~/[DatabaseName]/[TypeofPage]
- https://jackdaw.cam.ac.uk[:443,:8443]/~/[jackdaw,jdawdev.jdawtest,jdawtmp]/[t,-]
- https://jackdaw.cam.ac.uk/ same as https://jackdaw.cam.ac.uk:443/~/jackdaw/- Production server backed by production database and production pages 
- https://jackdaw.cam.ac.uk/t same as https://jackdaw.cam.ac.uk:443/~/jackdaw/t Production server backed by production database test pages
- https://jackdaw.cam.ac.uk:8443/ is the test server backed by production database (should be used with caution)
- https://jackdaw.cam.ac.uk/~/[jdawdev,jdawtest,jdawtmp] production server backed by develop/test/experiment DB - harmless good for what it is
- https://jackdaw.cam.ac.uk:8443/~/[jdawdev,jdawtest,jdawtmp]/t good for testing DB patches, httpdconfig and test pages 

Application repositories
------------------------
- Multiple RCS directories on the production server replicated to Standby Servers using bespoke Replication Manager

Technology
----------

| Category | Language | Framework |
| -------- | -------- | --------- |
| Server | Perl/v5.18.2 PerlToolKit PLSQL | Apache/2.2.34 (Unix)  mod_perl/2.0.8  |
| Client | Jquery | ?  |


Deployment
----------
- Shell Scripts on the Server 

Deployment repository
---------------------
- Multiple RCS directories on the production server replicated to Standby Servers using bespoke Replication Manager

Service Owner
-------------
[Vijay Samtani](https://www.lookup.cam.ac.uk/person/crsid/vkhs1)

Service Managers
----------------
[Dr Ujjwal Das](https://www.lookup.cam.ac.uk/person/crsid/ukd20)

Current Status
--------------
Live

Documentation
-------------
- [Jackdaw on Confluence](https://confluence.uis.cam.ac.uk/display/JAC/Jackdaw+Home)
- [Jackdaw - service definition](https://confluence.uis.cam.ac.uk/display/IAM/Jackdaw+-+service+definition)
- [Jackdaw UIS JIRA](https://jira.uis.cam.ac.uk/secure/Dashboard.jspa?selectPageId=11002)
- [Jackdaw HPC JIRA](https://jira.hpc.cam.ac.uk/secure/Dashboard.jspa?selectPageId=10500)