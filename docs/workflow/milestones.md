# Milestones

## What milestones represent

We use
[milestones](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/milestones)
in GitLab to represent Sprints. As such each milestone will have a start and an
end date corresponding to the start and end of the associated sprint.

**All** work associated with a sprint should be added to a milestone. When
addressing a GitLab issue make sure to add any related Merge Requests to the
milestone.

## Boards

Milestones have a
[board](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/boards)
associated with them. We have a board for the current sprint which lists all the
issues for that sprint.

!!! warning
    Merge Requests are not listed on the board. If you want someone to pick up
    and review your Merge Request in good time, make sure it has a related issue
    which appears on the board.
