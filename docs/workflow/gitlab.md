---
title: GitLab
---

We "[eat our own
dogfood](https://en.wikipedia.org/wiki/Eating_your_own_dog_food)" and use the
[GitLab instance](https://gitlab.developers.cam.ac.uk) on the Developers' Hub to
manage our work. All team members should be part of the [DevOps
group](https://gitlab.developers.cam.ac.uk/uis/devops/) on GitLab.

## Groups and projects

Each service has its own group on GitLab. These are usually sub-groups of the
DevOps group.

Within each service's group, there are usually one or more projects. Projects
can be roughly grouped into the following categories:

* **Support and documentation.** Projects which have repository hosting disabled
    and exist purely as wiki and/or issue tracking projects. These can be the
    "public" side of private repos if we want to offer public developer focussed
    documentation or a public issue tracker.
* **Deployment.** Projects which contain code to deploy a particular service.
    These usually only contain deployment configuration and secrets. They are
    almost always private as they contain deep details of how a service is
    deployed.
* **Implementation.** Projects which contain the implementation of services. We
    try to structure our services with the idea that they may be deployed in
    multiple places or by multiple people. As such the implementation of a
    service should not assume it will be deployed in a particular place or at a
    particular URL.

## Creating a new project

When creating new projects in GitLab we try to follow the conventions below.

### Open by default

Our software projects are Open Source by default. As such the GitLab project
should be made public except where:

1. It contains internal details of deployments. (These are usally the "deploy" repos.)
2. It is a project entirely devoted to tracking operational issues with a
   particular deployment where tier 3 issues need to be escalated.
3. If contains grandfathered code which has not been audited.

We run our projects as Open Source projects and so the code should be general,
understandable and written with the assumption it may be deployed in multiple
places. Try to avoid Cambridge-specific hard-coded values although
Cambridge-specific defaults are fine.

### License

Our default license is the [MIT-style
license](https://opensource.org/licenses/MIT).

### Where

Usually a project will be created within a sub-group of the
[DevOps group](https://gitlab.developers.cam.ac.uk/uis/devops/).

### Naming

Naming is hard. When providing the "short name" for a project, consider that it
will appear in URLS. As such it is better to have projects named like
``uis/devops/raven/deploy`` rather than ``uis/devops/raven/raven-deploy``.

The "long name" should make sense in isolation so, for the example above, naming
the project "Raven Deployment Configuration" makes sense.

### Settings

New projects should have the following settings changed:

* Require 1 approver for all Merge Requests

### Content

Create the project with a README. This provides an initial commit which forms
the ``master`` branch. The contents of the repository should be opened as the
first merge request for review.

### Default issue template

Under Settings > General > Default issue template, paste the following:

```md
**DO NOT POST PERSONAL OR CONFIDENTIAL DATA IN THIS PROJECT**

This project is hosting an Open Source project. By design the development
process is open to be inspected. Issues should be related to the code itself and
bugs which affect all deployments. Do not open issues relating only to a single
deployment.

It is useful if you follow [Simon Tatham's guide for reporting
bugs](https://www.chiark.greenend.org.uk/~sgtatham/bugs.html). At a minimum we
would need:

* Browser and Operating System vendor and version numbers.
* A description of what you were *trying* to achieve.
* A description of what you did in order to achieve it.
* A description of what you expected to happen.
* A description of what actually happened.

If we don't have enough information to reproduce your issue, we will probably
not be able to address it.
```
