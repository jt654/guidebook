---
title: Onboarding new starters
---

The following checklist is to be used as a reminder of the admin tasks needed to
onboard a new member to the DevOps team.

### Checklist

* Add to [DevOps lookup group](https://www.lookup.cam.ac.uk/group/uis-devops)
* Add to [devops@uis.cam.ac.uk mailing list](https://lists.cam.ac.uk/mailman/admin/uis-service-automation)
* Add to [GitLab group](https://gitlab.developers.cam.ac.uk/groups/uis/devops/-/group_members)
* Add to [Google Team drive](https://drive.google.com/drive/folders/0AJboYXmrsklAUk9PVA)
* Re-encrypt ansible vault passwords, see [setting up GPG to decrypt secrets](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/docs/secrets.md#setting-up-gpg-to-decrypt-secrets) for guidance
    * Add public GPG key to [team public keyring](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/files/teampubkeys.gpg)
    * Add long form key to [automation group list](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/docs/secrets.md#setting-up-gpg-to-encrypt-secrets)
    * Re-encrypt [docs repo's test secret](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/files/testsecret.gpg)
    * Re-encrypt [docs repo's ansible vault password](https://gitlab.developers.cam.ac.uk/uis/devops/docs/general/blob/master/secrets/secrets.password.asc)
    * _Re-encrypt other repos' ansible vault passwords on an "as needed" basis_
* Add public SSH key to [ansible add-devops-users role](https://gitlab.developers.cam.ac.uk/uis/devops/infra/ansible-roles/tree/master/roles/devops-ssh-users)
* Get new member to visit https://lists.cam.ac.uk/mailman/listinfo/uis-staff and request membership. Follow up with email to uis-staff-owner@lists.cam.ac.uk explaining who new hire is.
* If new member desires, get them to visit https://lists.cam.ac.uk/mailman/listinfo/uis-social and sign themselves up to the social list. No approval is required for this.

If working on Gitlab deployment and/or MWSv3:

* Add to [GitHub organisation](https://github.com/uisautomation) (_cost involved_)
